package dawson;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
   
    /**
     * Test :-)
     */
    @Test
    public void TestMethodEcho()
    {
      assertEquals("echo should return 5:",5, App.echo(5));
    }
    /**
     * Test :-)
     */
    @Test
    public void TestMethodOneMore()
    {
        assertEquals("echo should return 6:",6, App.oneMore(5));
    }

    
}
